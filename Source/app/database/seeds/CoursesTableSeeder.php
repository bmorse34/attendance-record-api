<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class CoursesTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 10) as $index)
		{
			Course::create([
                'title' => $faker->company,
                'description' => $faker->text,
                'absences_limit' => $faker->numberBetween(1,10),
                'alert_limit' => $faker->numberBetween(1, 10),
                'user_id' => $faker->numberBetween(1,10),
			]);
		}
	}

}