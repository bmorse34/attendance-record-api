<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class CourseStudentTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 10) as $index)
		{
			CourseStudent::create([
				'course_id' => $faker->numberBetween(1,10),
				'student_id' => $faker->numberBetween(1,10),
			]);
		}
	}

}