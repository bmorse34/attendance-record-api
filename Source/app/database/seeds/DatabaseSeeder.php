<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('CoursesTableSeeder');
		$this->call('StudentsTableSeeder');
		$this->call('UsersTableSeeder');
		$this->call('CourseStudentTableSeeder');
		$this->call('AbsencesTableSeeder');
	}

}
