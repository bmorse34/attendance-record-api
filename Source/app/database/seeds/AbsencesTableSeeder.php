<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class AbsencesTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 10) as $index)
		{
			Absence::create([
				'course_id' => $faker->numberBetween(1,10),
				'student_id' => $faker->numberBetween(1,10),
				'date_absent' => $faker->dateTimeThisCentury->format('Y-m-d'),
			]);
		}
	}

}