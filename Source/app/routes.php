<?php

Route::get('/chuck', function()
{
	return View::make('hello');
});


Route::get('/userhascourse/{id}', function($id) {
	var_dump(Course::user_has_course(1, $id));
});

Route::group(array('prefix' => 'api'), function()
{
	Route::get('student', array('before' => 'auth', 'uses' => 'StudentController@index'));
	Route::post('student', array('before' => 'auth', 'uses' => 'StudentController@store'));
	Route::get('student/{id}', array('before' => 'auth', 'uses' => 'StudentController@show'));
	Route::put('student/{id}', array('before' => 'auth', 'uses' => 'StudentController@update'));
	Route::delete('student/{id}', array('before' => 'auth', 'uses' => 'StudentController@destroy'));


	Route::get('course', array('before' => 'auth', 'uses' => 'CourseController@index'));
	Route::post('course', array('before' => 'auth', 'uses' => 'CourseController@store'));
	Route::get('course/{id}', array('before' => 'auth', 'uses' => 'CourseController@show'));
	Route::put('course/{id}', array('before' => 'auth', 'uses' => 'CourseController@update'));
	Route::delete('course/{id}', array('before' => 'auth', 'uses' => 'CourseController@destroy'));

	Route::get('course/{id}/students', array('before' => 'auth', 'uses' => 'CourseController@get_students_in_course'));

});