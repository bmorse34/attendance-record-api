<?php

class StudentController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /student
	 *
	 * @return Response
	 */
	public function index()
	{
		$students = Student::orderby('last_name', 'asc')->get();
		return Response::json($students, 200);
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /student
	 *
	 * @return Response
	 */
	public function store()
	{
        $student = Student::create([
            'first_name' => Input::get('first_name'),
            'last_name' => Input::get('last_name'),
        ]);

        return $student;
	}

	/**
	 * Display the specified resource.
	 * GET /student/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return Student::find($id);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /student/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$student = Student::find($id);

        $student->first_name = Input::get('first_name');
        $student->last_name = Input::get('last_name');

        $student->save();

        return $student;
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /student/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $student = Student::find($id);
        $student->delete();

        return $student;
	}
}