<?php

class CourseController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /course
	 *
	 * @return Response
	 */
	public function index()
	{
		$user_id = 4;
		$courses = Course::orderby('title', 'asc')->whereUserId($user_id)->get();

		if($courses->count())
			return $courses;

		return Response::json(array('message' => 'There are no courses for this user'), 204);

		// return Response::json($courses, 200);
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /course
	 *
	 * @return Response
	 */
	public function store()
	{
		$user_id = 2;

        $course = Course::create([
            'title' => Input::get('title'),
            'description' => Input::get('description'),
            'absences_limit' => Input::get('absences_limit'),
            'alert_limit' => Input::get('alert_limit'),
            'user_id' => $user_id,
        ]);

        return $course;
	}

	/**
	 * Display the specified resource.
	 * GET /course/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$user_id = 2;

		$course = Course::where('user_id', $user_id)->where('id', $id)->get();

		if($course->count())
			return $course;

		return Response::json(array('message' => 'This course does not exist for user'), 403);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /course/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$user_id = 2;

		$course = Course::find($id);

        $course->title = Input::get('title');
        $course->description = Input::get('description');
        $course->absences_limit = Input::get('absences_limit');
        $course->alert_limit = Input::get('alert_limit');
        $course->user_id = $user_id;

        $course->save();

        return $course;
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /course/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $course = Course::find($id);
        $course->delete();

        return $course;
	}

	public function get_students_in_course($id)
	{
		$students = Course::find($id)->students;
		// $id = 4;
		// $students = CourseStudent::get_students_in_course($id);
		return $students;
	}

}