<?php

class CourseStudent extends \Eloquent {
	protected $table = 'course_student';
	protected $fillable = [];

	public static function get_students_in_course($course_id) {

		// return array('howdy');
		$students = DB::table('students')->where('courses_students.course_id', '=', $course_id)->join('courses_students', 'students.id = courses_students.student_id')->get();
		
		return $students;
	}
}