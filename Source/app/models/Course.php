<?php

class Course extends \Eloquent {
	protected $fillable = [];

	public function students()
    {
    	return $this->belongsToMany('Student');
    }

	public function users()
    {
    	return $this->belongsToMany('User');
    }

    public static function user_has_course($user_id, $course_id)
    {
    	// $user_id = Session::get('user_id');
    	$user_id = 2;
    	return DB::table('course_student')->where('user_id', $user_id)->where('course_id', $course_id)->get();
    }

}